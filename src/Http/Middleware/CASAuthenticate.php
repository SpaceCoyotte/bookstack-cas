<?php

namespace SpaceCoyotte\Cas\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use BookStack\Auth\User;
use BookStack\Auth\Role;

class CASAuthenticate
{

    protected $auth;
    protected $cas;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->cas = app('cas');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->ajax() || $request->wantsJson()) {

            return response('Unauthorized.', 401);

        } else {

            // First authenticate and let's see what happens
            $this->cas->authenticate();

            // If it seems to have met success, get user name and other attributes
            if ( $this->cas->isAuthenticated() ) {

                // NOTE! A Bookstack's user unicity relies on its 'email' attribute. $username MUST be unique in your CAS's directory and having it not matching an email address pattern was not tested. It is recommended to set your CAS server to send an unique email address information as user subject identifier
                $username = $this->cas->getUser();
                $remember = false;

                $otherAttributes = $this->cas->getAttributes();

                // Read user's groups (from CAS's directory) to Bookstack's roles mapping information (see Config/cas.php file)
                $rolesGroupsMapping = config('cas.rolesGroupsMapping');
                $groupsFromCAS = !empty( $otherAttributes[config('cas.groupsAttributeName')] ) ? explode(',', $otherAttributes[config('cas.groupsAttributeName')] ) : [];
                $availableRoles = Role::all()->pluck('display_name')->toArray();

                // Init a role list to attach the user to
                $roles = [];

                // Actually maps groups to roles upon given patterns (see Config/cas.php file)
                foreach ($rolesGroupsMapping as $role => $group) {
                    if ( !empty( preg_grep($group, $groupsFromCAS)) && in_array($role, $availableRoles) ) {
                        $roles[] = $role;
                    }
                }

                // Building displayName from config
                $displayNameParts = config('cas.displayNameAttributesNames');
                $displayName= '';
                foreach ($displayNameParts as $part) {
                    $displayName .= ' ' . ( !empty( $otherAttributes[$part] ) ? $otherAttributes[$part] : '' );
                }
                $displayName = trim( $displayName );

                // Load the user and create it if needed
                // NOTE! A Bookstack's user unicity relies on its 'email' attribute. $username MUST be unique in your CAS's directory and having it not matching an email address pattern was not tested. It is recommended to set your CAS server to send an unique email address information as user subject identifier
                $user = User::firstOrCreate([
                    'email' => $username,
                ]);

                // If the user has no name, it's most probably a new one. Set its name using username as fallback value
                if ( empty( $user->name ) ) {
                    $user->name = strlen( $displayName ) > 1 ? $displayName : $username;
                    $user->save();

                    // Attach default roles to new user if configred so
                    $defaultRoleId = config('cas.defaultRoleId', 'public');
                    if ( config('cas.attachDefaultRole') && setting('registration-role') ) {
                        $roles[] = Role::where(['name' => $defaultRoleId])>first();
                    }

                }

                // Clean up roles list
                $roles = array_unique($roles);

                // Actually attach roles to user
                foreach ($roles as $roleDisplayName) {
                    if( !(bool) $user->roles()->where(['display_name' => $roleDisplayName])->count() ) {
                        $user->attachRole(Role::getRole($roleDisplayName));
                    }
                }

                // Log the user in
                $this->auth->login($user, $remember);

            }

        }

        return $next($request);
    }

}
