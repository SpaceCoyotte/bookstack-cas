# Bookstack simple CAS authentication middleware

## First of all
Please be mercifull :  this is my first package... Most of this package's files were barely copied from `sentrasoft/laravel-cas`. Thank you [Sentrasoft Guys](https://github.com/sentrasoft)!

## Usage
1) Require this package into your Bookstack root directory : 
```bash
composer require spacecoyotte/bookstack-cas:dev-master
```

2) Add `SpaceCoyotte\Cas\CasServiceProvider::class,` in your `app/Config/app.php` `providers` array
```php
// ...
// Application Services Provides
'providers' => [

    // Laravel Framework Service Providers...
    Illuminate\Auth\AuthServiceProvider::class,
	// ...
    BookStack\Providers\CustomFacadeProvider::class,
    
    // Contributed Service Providers
    SpaceCoyotte\Cas\CasServiceProvider::class,
],
// ...
```

3) Publish vendor's assets : 
```bash
php artisan vendor:publish
```

4) Edit `app/Config/cas.php` to match your requirements

5) Edit `app/Http/Kernel.php` file to add `cas.auth` group middleware:
```php
// ...
    protected $routeMiddleware = [
        // ...
        'auth.cas'   => \SpaceCoyotte\Cas\Middleware\CASAuthenticate::class,
        // ...
    ];
// ...
```

6) Swap `auth` widdleware to `auth.cas` in your `routes/web.php` file:
```php
/// ...
// Authenticated routes...
Route::group(['middleware' => 'cas.auth'], function () {
/// ...
```

7) Et voilà!